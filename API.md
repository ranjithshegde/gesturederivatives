# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`gestureFinder`](#classgestureFinder) | 
`class `[`ofApp`](#classofApp) | 
`struct `[`gestureFinder::derivatives`](#structgestureFinder_1_1derivatives) | A simple data structure to contain all relevant derivative values `[gestureFinder::derivatives::firstDt](#structgestureFinder_1_1derivatives_1aa1769310478981840f69f791a38dbb4a)` and `[gestureFinder::derivatives::secondDt](#structgestureFinder_1_1derivatives_1a5f26b0cd447e05228b7eb9e732041610)` store the velocity and acceleration respectively `[gestureFinder::derivatives::oldFirst](#structgestureFinder_1_1derivatives_1ac8c4883c650657a7de25300ab2308191)` and `[gestureFinder::derivatives::oldSecond](#structgestureFinder_1_1derivatives_1ab9c4e9d12dc70099f6ac6f5a6aa582c1)` store the previous values for velocity and acceleration respectively `[gestureFinder::derivatives::period](#structgestureFinder_1_1derivatives_1a39cd472a464f2ec2c968099346424c40)` sets the measurement period for average calculation `[gestureFinder::derivatives::averageDt](#structgestureFinder_1_1derivatives_1a0b989e66b086dc849a30dc1c0b2bbd6d)` and `[gestureFinder::derivatives::averageSecondDt](#structgestureFinder_1_1derivatives_1a3b050f1777cb385537236a5adbe8b468)` store the average velocity and acceleration respectively

# class `gestureFinder` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public  `[`gestureFinder`](#classgestureFinder_1abc572d93cc58d4af04889c33a2267833)`()` | This class contains a set of tools to calculate motion derivatives for values of `float` or `glm::vec2` Extend or overload the member functions to work with higher order vectors such as `glm::vec3` following the overloaded example of `[derive(glm::vec2)](#classgestureFinder_1a72f104d2680cedb91d3b391b0e042c71)` member function
`public  `[`~gestureFinder`](#classgestureFinder_1ad36985cbebe1477be0c160f2ea6d2abf)`()` | 
`public void `[`derive`](#classgestureFinder_1a62d783f0c42324fda2eaeb60813e7d91)`(float value)` | Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop. It stores the previous value and compares it against the current value
`public void `[`derive`](#classgestureFinder_1a72f104d2680cedb91d3b391b0e042c71)`(glm::vec2 value)` | Generate and store the derivatives for the given 2D value i.e glm::vec2. This member function is ideally called inside a `while` loop. It stores the previous value and compares it against the current value
`public void `[`average`](#classgestureFinder_1a20a4964592614a0c339f860d4f6f7f7e)`()` | Calculates and stores the average for the value set in `[gestureFinder::derive](#classgestureFinder_1a62d783f0c42324fda2eaeb60813e7d91)`. The time period for measurement is set from `gestureFinder::m_Deriv.period` which can be set from `[gestureFinder::setPeriod](#classgestureFinder_1a5312255037e591b5aa51c947f54dfda1)`
`public float `[`velocity`](#classgestureFinder_1ace9698eecaeb1f64bead9bc3e7f3851e)`()` | Returns the current velocity calculated from the most recent `[gestureFinder](#classgestureFinder):derive` call.
`public float `[`acceleartion`](#classgestureFinder_1a83736e41ad23b7615d4889698d5af8b8)`()` | Returns the current acceleartion calculated from the most recent `[gestureFinder](#classgestureFinder):derive` call.
`public float `[`oldValue`](#classgestureFinder_1a6b0b2b9a1954dcaa03b9743d4969518f)`()` | Returns the previous velocity calculated from the most recent `[gestureFinder](#classgestureFinder):derive` call.
`public float `[`oldDt`](#classgestureFinder_1ac647a2f2d009c092009e60120a3817c4)`()` | Returns the previous acceleration calculated from the most recent `[gestureFinder](#classgestureFinder):derive` call.
`public inline void `[`setPeriod`](#classgestureFinder_1a5312255037e591b5aa51c947f54dfda1)`(unsigned int period)` | Sets the period for the calculation of average value by `[gestureFinder::average](#classgestureFinder_1a20a4964592614a0c339f860d4f6f7f7e)`
`public double `[`avgVelocity`](#classgestureFinder_1a8d39c623a78dea8aa04722eb7f79e4b4)`()` | Returns the average velocity calculated by `[gestureFinder::average](#classgestureFinder_1a20a4964592614a0c339f860d4f6f7f7e)`
`public double `[`avgAccel`](#classgestureFinder_1a4cd5d07b126fd93185144d6e3fed9b1f)`()` | Returns the average acceleartion calculated by `[gestureFinder::average](#classgestureFinder_1a20a4964592614a0c339f860d4f6f7f7e)`

## Members

#### `public  `[`gestureFinder`](#classgestureFinder_1abc572d93cc58d4af04889c33a2267833)`()` 

This class contains a set of tools to calculate motion derivatives for values of `float` or `glm::vec2` Extend or overload the member functions to work with higher order vectors such as `glm::vec3` following the overloaded example of `[derive(glm::vec2)](#classgestureFinder_1a72f104d2680cedb91d3b391b0e042c71)` member function

#### `public  `[`~gestureFinder`](#classgestureFinder_1ad36985cbebe1477be0c160f2ea6d2abf)`()` 

#### `public void `[`derive`](#classgestureFinder_1a62d783f0c42324fda2eaeb60813e7d91)`(float value)` 

Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop. It stores the previous value and compares it against the current value

#### `public void `[`derive`](#classgestureFinder_1a72f104d2680cedb91d3b391b0e042c71)`(glm::vec2 value)` 

Generate and store the derivatives for the given 2D value i.e glm::vec2. This member function is ideally called inside a `while` loop. It stores the previous value and compares it against the current value

#### `public void `[`average`](#classgestureFinder_1a20a4964592614a0c339f860d4f6f7f7e)`()` 

Calculates and stores the average for the value set in `[gestureFinder::derive](#classgestureFinder_1a62d783f0c42324fda2eaeb60813e7d91)`. The time period for measurement is set from `gestureFinder::m_Deriv.period` which can be set from `[gestureFinder::setPeriod](#classgestureFinder_1a5312255037e591b5aa51c947f54dfda1)`

#### `public float `[`velocity`](#classgestureFinder_1ace9698eecaeb1f64bead9bc3e7f3851e)`()` 

Returns the current velocity calculated from the most recent `[gestureFinder](#classgestureFinder):derive` call.

#### `public float `[`acceleartion`](#classgestureFinder_1a83736e41ad23b7615d4889698d5af8b8)`()` 

Returns the current acceleartion calculated from the most recent `[gestureFinder](#classgestureFinder):derive` call.

#### `public float `[`oldValue`](#classgestureFinder_1a6b0b2b9a1954dcaa03b9743d4969518f)`()` 

Returns the previous velocity calculated from the most recent `[gestureFinder](#classgestureFinder):derive` call.

#### `public float `[`oldDt`](#classgestureFinder_1ac647a2f2d009c092009e60120a3817c4)`()` 

Returns the previous acceleration calculated from the most recent `[gestureFinder](#classgestureFinder):derive` call.

#### `public inline void `[`setPeriod`](#classgestureFinder_1a5312255037e591b5aa51c947f54dfda1)`(unsigned int period)` 

Sets the period for the calculation of average value by `[gestureFinder::average](#classgestureFinder_1a20a4964592614a0c339f860d4f6f7f7e)`

#### `public double `[`avgVelocity`](#classgestureFinder_1a8d39c623a78dea8aa04722eb7f79e4b4)`()` 

Returns the average velocity calculated by `[gestureFinder::average](#classgestureFinder_1a20a4964592614a0c339f860d4f6f7f7e)`

#### `public double `[`avgAccel`](#classgestureFinder_1a4cd5d07b126fd93185144d6e3fed9b1f)`()` 

Returns the average acceleartion calculated by `[gestureFinder::average](#classgestureFinder_1a20a4964592614a0c339f860d4f6f7f7e)`

# class `ofApp` 

```
class ofApp
  : public ofBaseApp
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public glm::vec2 `[`pos`](#classofApp_1a8009a6db11ec8bb6c15310effa6929a3) | 
`public float `[`speed`](#classofApp_1a9a8399af45f810d1650cc102c1245c16) | 
`public float `[`acc`](#classofApp_1acf63efbeaa62bd78d74d13dde1e23035) | 
`public float `[`newAcc`](#classofApp_1a6d8826ec74a80deb61bae0d958c3ac2a) | 
`public float `[`radius`](#classofApp_1abccf0e70d1e0d79437d05bd67f9c3c16) | 
`public bool `[`smoothPos`](#classofApp_1a9235a71127c4c89e7a173a5c929e74ec) | 
`public bool `[`smoothRadius`](#classofApp_1a8b74bcc8ee40a20588459dec87a2ba50) | 
`public int `[`counter`](#classofApp_1adea31a22af9cfa606efb97ff1dc29ee3) | 
`public double `[`AccumAccel`](#classofApp_1aa16c6c5e7dfd2cfb4fd43e708be5931d) | 
`public double `[`AccumSpeed`](#classofApp_1a54b06abbf31a27aa86221f40e4a05c68) | 
`public vector< glm::vec2 > `[`change`](#classofApp_1a9f21644e29bc9caf3c794d879e543154) | 
`public int `[`iterator`](#classofApp_1aafb190f8c6b197bf90f9fbc2eb742269) | 
`public float `[`maxX`](#classofApp_1aefb344dc39e2d29069c20b81e969b915) | 
`public float `[`maxY`](#classofApp_1ae563e2545a271b7bba02173f33df9f12) | 
`public bool `[`L`](#classofApp_1afa6121ebf997b09c7b54e264d07456ec) | 
`public float `[`pressTime`](#classofApp_1aa03ad6330704f72e195e7e074549650c) | 
`public float `[`currentMax`](#classofApp_1a098c7050aaa1918936e85bb68712308e) | 
`public float `[`currentMin`](#classofApp_1afb907ca544b290b60bc97fae48cdafba) | 
`public float `[`timeRad`](#classofApp_1ac64d93447df300ce3aba6cd9767ef63b) | 
`public float `[`newSp`](#classofApp_1ae9ad083f461514a36a35a9d65503d612) | 
`public float `[`newAccel`](#classofApp_1a4f96aeff8ea776d6d6c10e49861ceb55) | 
`public vector< float > `[`times`](#classofApp_1a5be64c5eefa6db00ba9f3dc81edbc6d3) | 
`public `[`gestureFinder`](#classgestureFinder)` `[`mouseGestures`](#classofApp_1a45493cde5945a110e522c4c2b1fc14a1) | 
`public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` | 
`public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` | Initiate recording of mouse movements as gesutre only when `MOUSE_BUTTON_LEFT` is pressed.
`public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` | The colour of the radious is red for large mouse strokes and green for short strokes Draw small filled circles to indicate the duration of each stroke. The position is determined by the rate of change of duration The radious is determined using either hte minimum or the maximum duration for the current period
`public void `[`keyPressed`](#classofApp_1a957d3197364bbac8e67eaa4f15b28ad3)`(int key)` | Use keys "p" and "r" to toggle position smoothening and radius smoothening respectively.
`public void `[`mousePressed`](#classofApp_1a2c2ea9c160231e55424dfd98466ef27d)`(int x,int y,int button)` | Start the timer and start drawing the line on every mouse press.
`public void `[`mouseReleased`](#classofApp_1aa3131f1554fc49eaa9ee0f284e48129b)`(int x,int y,int button)` | Sop the timer and stop drawing the line on every mouse release.

## Members

#### `public glm::vec2 `[`pos`](#classofApp_1a8009a6db11ec8bb6c15310effa6929a3) 

#### `public float `[`speed`](#classofApp_1a9a8399af45f810d1650cc102c1245c16) 

#### `public float `[`acc`](#classofApp_1acf63efbeaa62bd78d74d13dde1e23035) 

#### `public float `[`newAcc`](#classofApp_1a6d8826ec74a80deb61bae0d958c3ac2a) 

#### `public float `[`radius`](#classofApp_1abccf0e70d1e0d79437d05bd67f9c3c16) 

#### `public bool `[`smoothPos`](#classofApp_1a9235a71127c4c89e7a173a5c929e74ec) 

#### `public bool `[`smoothRadius`](#classofApp_1a8b74bcc8ee40a20588459dec87a2ba50) 

#### `public int `[`counter`](#classofApp_1adea31a22af9cfa606efb97ff1dc29ee3) 

#### `public double `[`AccumAccel`](#classofApp_1aa16c6c5e7dfd2cfb4fd43e708be5931d) 

#### `public double `[`AccumSpeed`](#classofApp_1a54b06abbf31a27aa86221f40e4a05c68) 

#### `public vector< glm::vec2 > `[`change`](#classofApp_1a9f21644e29bc9caf3c794d879e543154) 

#### `public int `[`iterator`](#classofApp_1aafb190f8c6b197bf90f9fbc2eb742269) 

#### `public float `[`maxX`](#classofApp_1aefb344dc39e2d29069c20b81e969b915) 

#### `public float `[`maxY`](#classofApp_1ae563e2545a271b7bba02173f33df9f12) 

#### `public bool `[`L`](#classofApp_1afa6121ebf997b09c7b54e264d07456ec) 

#### `public float `[`pressTime`](#classofApp_1aa03ad6330704f72e195e7e074549650c) 

#### `public float `[`currentMax`](#classofApp_1a098c7050aaa1918936e85bb68712308e) 

#### `public float `[`currentMin`](#classofApp_1afb907ca544b290b60bc97fae48cdafba) 

#### `public float `[`timeRad`](#classofApp_1ac64d93447df300ce3aba6cd9767ef63b) 

#### `public float `[`newSp`](#classofApp_1ae9ad083f461514a36a35a9d65503d612) 

#### `public float `[`newAccel`](#classofApp_1a4f96aeff8ea776d6d6c10e49861ceb55) 

#### `public vector< float > `[`times`](#classofApp_1a5be64c5eefa6db00ba9f3dc81edbc6d3) 

#### `public `[`gestureFinder`](#classgestureFinder)` `[`mouseGestures`](#classofApp_1a45493cde5945a110e522c4c2b1fc14a1) 

#### `public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` 

#### `public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` 

Initiate recording of mouse movements as gesutre only when `MOUSE_BUTTON_LEFT` is pressed.

Start the timer 
```cpp
pressTime = ofGetElapsedTimef();
```
 Rate of Change 
```cpp
glm::vec2 newPos(ofGetMouseX(), ofGetMouseY());
```
 Use a smoothening function to reduce the drastic changes in size 
```cpp
if (smoothPos) {
   newPos = 0.97f * pos + 0.03f * newPos;
}
```
 Start the derivation (in a while loop) 
```cpp
mouseGestures.[derive](#classgestureFinder_1a62d783f0c42324fda2eaeb60813e7d91)(newPos);
```
 Calculate the average speed and acceleration using: 
```cpp
if (ofGetFrameNum() % 60 == 0) {
    newAccel = ofMap(mouseGestures.[avgAccel](#classgestureFinder_1a4cd5d07b126fd93185144d6e3fed9b1f)(), -15.0, 10.0, 0, ofGetWidth(), true);
    newSp = ofMap(mouseGestures.[avgVelocity](#classgestureFinder_1a8d39c623a78dea8aa04722eb7f79e4b4)(), 0.0, 700, 0, ofGetHeight(), true);
    // glm::vec2 newP(newAccel, newSp);
    change.push_back(glm::vec2(newAccel, newSp));
    iterator++;
}
```

#### `public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` 

The colour of the radious is red for large mouse strokes and green for short strokes Draw small filled circles to indicate the duration of each stroke. The position is determined by the rate of change of duration The radious is determined using either hte minimum or the maximum duration for the current period

#### `public void `[`keyPressed`](#classofApp_1a957d3197364bbac8e67eaa4f15b28ad3)`(int key)` 

Use keys "p" and "r" to toggle position smoothening and radius smoothening respectively.

#### `public void `[`mousePressed`](#classofApp_1a2c2ea9c160231e55424dfd98466ef27d)`(int x,int y,int button)` 

Start the timer and start drawing the line on every mouse press.

#### `public void `[`mouseReleased`](#classofApp_1aa3131f1554fc49eaa9ee0f284e48129b)`(int x,int y,int button)` 

Sop the timer and stop drawing the line on every mouse release.

# struct `gestureFinder::derivatives` 

A simple data structure to contain all relevant derivative values `[gestureFinder::derivatives::firstDt](#structgestureFinder_1_1derivatives_1aa1769310478981840f69f791a38dbb4a)` and `[gestureFinder::derivatives::secondDt](#structgestureFinder_1_1derivatives_1a5f26b0cd447e05228b7eb9e732041610)` store the velocity and acceleration respectively `[gestureFinder::derivatives::oldFirst](#structgestureFinder_1_1derivatives_1ac8c4883c650657a7de25300ab2308191)` and `[gestureFinder::derivatives::oldSecond](#structgestureFinder_1_1derivatives_1ab9c4e9d12dc70099f6ac6f5a6aa582c1)` store the previous values for velocity and acceleration respectively `[gestureFinder::derivatives::period](#structgestureFinder_1_1derivatives_1a39cd472a464f2ec2c968099346424c40)` sets the measurement period for average calculation `[gestureFinder::derivatives::averageDt](#structgestureFinder_1_1derivatives_1a0b989e66b086dc849a30dc1c0b2bbd6d)` and `[gestureFinder::derivatives::averageSecondDt](#structgestureFinder_1_1derivatives_1a3b050f1777cb385537236a5adbe8b468)` store the average velocity and acceleration respectively

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public float `[`firstDt`](#structgestureFinder_1_1derivatives_1aa1769310478981840f69f791a38dbb4a) | Current velocity.
`public float `[`secondDt`](#structgestureFinder_1_1derivatives_1a5f26b0cd447e05228b7eb9e732041610) | Current acceleration.
`public float `[`oldFirst`](#structgestureFinder_1_1derivatives_1ac8c4883c650657a7de25300ab2308191) | Previous velocity.
`public float `[`oldSecond`](#structgestureFinder_1_1derivatives_1ab9c4e9d12dc70099f6ac6f5a6aa582c1) | Previous acceleration.
`public unsigned int `[`period`](#structgestureFinder_1_1derivatives_1a39cd472a464f2ec2c968099346424c40) | Duration to meaure averages.
`public unsigned int `[`count`](#structgestureFinder_1_1derivatives_1a09f3ef33587c8f718f07248423f7f055) | Current index in the duration cycle.
`public double `[`averageDt`](#structgestureFinder_1_1derivatives_1a0b989e66b086dc849a30dc1c0b2bbd6d) | Average velocity.
`public double `[`averageSecondDt`](#structgestureFinder_1_1derivatives_1a3b050f1777cb385537236a5adbe8b468) | Average acceleration.

## Members

#### `public float `[`firstDt`](#structgestureFinder_1_1derivatives_1aa1769310478981840f69f791a38dbb4a) 

Current velocity.

#### `public float `[`secondDt`](#structgestureFinder_1_1derivatives_1a5f26b0cd447e05228b7eb9e732041610) 

Current acceleration.

#### `public float `[`oldFirst`](#structgestureFinder_1_1derivatives_1ac8c4883c650657a7de25300ab2308191) 

Previous velocity.

#### `public float `[`oldSecond`](#structgestureFinder_1_1derivatives_1ab9c4e9d12dc70099f6ac6f5a6aa582c1) 

Previous acceleration.

#### `public unsigned int `[`period`](#structgestureFinder_1_1derivatives_1a39cd472a464f2ec2c968099346424c40) 

Duration to meaure averages.

#### `public unsigned int `[`count`](#structgestureFinder_1_1derivatives_1a09f3ef33587c8f718f07248423f7f055) 

Current index in the duration cycle.

#### `public double `[`averageDt`](#structgestureFinder_1_1derivatives_1a0b989e66b086dc849a30dc1c0b2bbd6d) 

Average velocity.

#### `public double `[`averageSecondDt`](#structgestureFinder_1_1derivatives_1a3b050f1777cb385537236a5adbe8b468) 

Average acceleration.

Generated by [Moxygen](https://sourcey.com/moxygen)
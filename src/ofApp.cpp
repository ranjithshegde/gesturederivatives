#include "ofApp.h"

//----------------------------------------------------------------------
void ofApp::setup()
{
    ofSetBackgroundColor(ofColor::black);
    ofSetBackgroundAuto(false);

    counter = 0;
    AccumAccel = 0.0;
    AccumSpeed = 0.0;
    iterator = 0;
    //! Set the period for measuremnet of averages
    mouseGestures.setPeriod(60);

    sampleRate = 48000;
    bufferSize = 1024;

    inL.assign(bufferSize, 0.0);
    inR.assign(bufferSize, 0.0);

    // auto devices = soundStream.getMatchingDevices("default");
    // if (!devices.empty()) {
    // 	settings.setInDevice(devices[2]);
    // 	settings.setOutDevice(devices[2]);
    // }

    settings.setOutListener(this);
    settings.setInListener(this);
    settings.sampleRate = sampleRate;
    settings.bufferSize = bufferSize;
    // settings.numBuffers = 2;
    settings.numOutputChannels = 2;
    settings.numInputChannels = 2;

    xn_1 = 0;
    xnp_1 = 0;
    yn_1 = 0;
    ynp_1 = 0;

    soundStream.setup(settings);
}

//----------------------------------------------------------------------
//! Initiate recording of mouse movements as gesutre only when `MOUSE_BUTTON_LEFT` is pressed

//! Start the timer
//! ```cpp
//! pressTime = ofGetElapsedTimef();
//! ```

//! Rate of Change
//! ```cpp
//! glm::vec2 newPos(ofGetMouseX(), ofGetMouseY());
//! ```

//! Use a smoothening function to reduce the drastic changes in size
//! ```cpp
//! if (smoothPos) {
//!    newPos = 0.97f * pos + 0.03f * newPos;
//! }
//! ```

//! Start the derivation (in a while loop)
//! ```cpp
//! mouseGestures.derive(newPos);
//! ```

//! Calculate the average speed and acceleration using:
//! ```cpp
//!        if (ofGetFrameNum() % 60 == 0) {
//!            newAccel = ofMap(mouseGestures.avgAccel(), -15.0, 10.0, 0, ofGetWidth(), true);
//!            newSp = ofMap(mouseGestures.avgVelocity(), 0.0, 700, 0, ofGetHeight(), true);
//!            // glm::vec2 newP(newAccel, newSp);
//!            change.push_back(glm::vec2(newAccel, newSp));
//!            iterator++;
//!        }
//! ```

void ofApp::update()
{
    // Initiate recording of mouse movements as gesutre only when `MOUSE_BUTTON_LEFT` is pressed
    if (ofGetMousePressed(OF_MOUSE_BUTTON_LEFT)) {

        // Start the timer
        pressTime = ofGetElapsedTimef();

        // Rate of Change
        glm::vec2 newPos(ofGetMouseX(), ofGetMouseY());

        // Use a smoothening function to reduce the drastic changes in size
        if (smoothPos) {
            newPos = 0.97f * pos + 0.03f * newPos;
        }

        // Start the derivation (in a while loop)
        mouseGestures.derive(newPos);
        mouseGestures.average();
        speed = mouseGestures.velocity();
        acc = mouseGestures.acceleartion();

        // implementation
        if (smoothRadius) {
            radius = 0.97f * radius + 0.03f * acc * 50.f;
        } else {
            radius = acc * 10.f;
        }
        pos = newPos;

        // average speed, acc
        if (ofGetFrameNum() % 60 == 0) {
            newAccel = ofMap(mouseGestures.avgAccel(), -15.0, 10.0, 0, ofGetWidth(), true);
            newSp = ofMap(mouseGestures.avgVelocity(), 0.0, 700, 0, ofGetHeight(), true);
            // glm::vec2 newP(newAccel, newSp);
            change.push_back(glm::vec2(newAccel, newSp));
            iterator++;
        }
    }

    //! min max time
    if (times.size() > 1) {
        currentMax = *max_element(times.begin(), times.end());
        currentMin = *min_element(times.begin(), times.end());
    }
}

//----------------------------------------------------------------------
//! The colour of the radious is red for large mouse strokes and green for short strokes
//! Draw small filled circles to indicate the duration of each stroke. The position is determined by the rate of change of duration
//! The radious is determined using either hte minimum or the maximum duration for the current period
void ofApp::draw()
{
    ofNoFill();
    // The colour of the radious is red for large mouse strokes and green for short strokes
    ofSetColor(radius > 0.f ? ofColor::red : ofColor::green);
    ofDrawCircle(pos, radius);
    ofSetColor(255);

    timeRad = ofRandom(1.0);

    // Draw small filled circles to indicate the duration of each stroke. The position is determined by the rate of change of duration
    // The radious is determined using either hte minimum or the maximum duration for the current period
    if (iterator > 1) {
        ofSetColor(20, 80, 245);
        ofFill();
        ofDrawCircle(change[iterator - 1], timeRad > 0.5 ? currentMin : currentMax);
        ofSetColor(250);
        ofDrawBitmapString("Current shortest time:  " + ofToString(currentMin) + "              Current longest time: " + ofToString(currentMax), 1000.f, ofGetHeight() - 30.f);
        ofDrawBitmapString("Current average speed: 	" + ofToString(newSp) + " 		Current average acceleration: 		" + ofToString(newAccel), 10.f, ofGetHeight() - 200.f);
    }

    if (smoothPos) {
        ofDrawBitmapString("Smooth position ON - press <p>", 10.f, 20.f);
    } else {
        ofDrawBitmapString("Smooth position OFF - press <p>", 10.f, 20.f);
    }

    if (smoothRadius) {
        ofDrawBitmapString("Smooth radius ON - press <r>", 400.f, 20.f);
    } else {
        ofDrawBitmapString("Smooth radius OFF - press <r>", 400.f, 20.f);
    }

    ofDrawBitmapString("Press mouse button to clear", 10.f, 40.f);
}

void ofApp::audioIn(ofSoundBuffer& input)
{
    for (size_t i = 0; i < input.getNumFrames(); i++) {
        inL[i] = input[i * input.getNumChannels()];
        inR[i] = input[i * input.getNumChannels() + 1];
    }
}

//--------------------------------------------------------------
void ofApp::audioOut(ofSoundBuffer& buffer)
{
    if (play) {
        float currentSample_1 = 0;
        float currentSample_2 = 0;
        for (size_t i = 1; i < buffer.getNumFrames(); i++) {

            // float weight_a = ofRandom(0.3f);
            // float weight_b = ofRandom(0.2f);
            // float weight_c = ofRandom(0.05f);
            // float weight_d = ofRandom(0.45f);
            weight_a = ofMap(mouseGestures.avgAccel(), -15.0, 10.0, -1.f, 1.f, true);
            weight_b = ofMap(mouseGestures.avgVelocity(), 0.0, 700, -1.f, 1.f, true);
            weight_c = ofMap(radius, 0.0, 20.f, -1.f, 1.0f, true);
            weight_d = ofMap(currentMin, 0.0, 700, -1.f, 1.0f, true);

            xn_1 = inL[i];
            currentSample_1 = (xn_1 * weight_d) + (xnp_1 * weight_a) + xn2p_1 - (yn_1 * weight_c) - (ynp_1 * weight_b);
            xn2p_1 = xnp_1;
            ynp_1 = yn_1;
            xnp_1 = xn_1;
            yn_1 = currentSample_1;
            currentSample_1 = 0.97 * currentSample_1 + 0.03 * yn_1;

            xn_2 = inR[i];
            currentSample_2 = (xn_2 * weight_d) + (xnp_2 * weight_a) + xn2p_2 - (yn_2 * weight_c) - (ynp_2 * weight_b);
            xn2p_2 = xnp_2;
            ynp_2 = yn_2;
            xnp_2 = xn_2;
            yn_2 = currentSample_2;

            currentSample_2 = 0.97 * currentSample_2 + 0.03 * yn_2;
            //!    newPos = 0.97f * pos + 0.03f * newPos;

            currentSample_1 = ofClamp(currentSample_1, 0.0f, 1.0f);
            currentSample_2 = ofClamp(currentSample_2, 0.0f, 1.0f);
            if (currentMin < 0.5f) {
                currentSample_1 = ofNoise(ofGetElapsedTimef());
                currentSample_2 = ofNoise(ofGetElapsedTimef() * 200);
            }
            // buffer[i * buffer.getNumChannels() + 0] = currentSample_1;
            // buffer[i * buffer.getNumChannels() + 1] = currentSample_2;
            buffer.getSample(i, 0) = currentSample_1;
            buffer.getSample(i, 1) = currentSample_2;
        }
    }
}

//----------------------------------------------------------------------
//! Use keys "p" and "r" to toggle position smoothening and radius smoothening respectively
void ofApp::keyPressed(int key)
{
    // Use keys "p" and "r" to toggle position smoothening and radius smoothening respectively
    if (key == 'p') {
        smoothPos = !smoothPos;
    }
    if (key == 'r') {
        smoothRadius = !smoothRadius;
    }
    if (key == ' ') {
        play = !play;
    }
}

//----------------------------------------------------------------------
//! Start the timer and start drawing the line on every mouse press.
void ofApp::mousePressed(int x, int y, int button)
{
    // Start the timer and start drawing the line on every mouse press.
    if (button == OF_MOUSE_BUTTON_RIGHT) {
        ofClear(ofColor::black);
        times.clear();
    }
}

//----------------------------------------------------------------------
//! Sop the timer and stop drawing the line on every mouse release.
void ofApp::mouseReleased(int x, int y, int button)
{
    // Sop the timer and stop drawing the line on every mouse release.
    if (button == OF_MOUSE_BUTTON_LEFT) {
        ofResetElapsedTimeCounter();
        times.push_back(pressTime);
    }
}


#pragma once

#include "gestureFinder.h"

class ofApp : public ofBaseApp {
public:
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);

    glm::vec2 pos;
    float speed = 0.f;
    float acc = 0.f;
    float newAcc = 0.f;
    float radius = 0.f;

    bool smoothPos = true;
    bool smoothRadius = true;
    int counter;
    double AccumAccel, AccumSpeed;
    vector<glm::vec2> change;
    int iterator;
    float maxX, maxY;
    bool L;

    float pressTime;
    float currentMax, currentMin, timeRad, newSp, newAccel;
    vector<float> times;

    gestureFinder mouseGestures;

    void audioIn(ofSoundBuffer& input);
    void audioOut(ofSoundBuffer& buffer);

    ofSoundStream soundStream;
    ofSoundStreamSettings settings;
    int bufferSize;
    int sampleRate;
    vector<float> inL;
    vector<float> inR;

    float xn_1, xnp_1, xn2p_1, yn_1, ynp_1, yn2p_1;
    float xn_2, xnp_2, xn2p_2, yn_2, ynp_2, yn2p_2;
    bool play = false;
    float weight_a, weight_b, weight_c, weight_d;
};

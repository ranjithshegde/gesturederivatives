#include "ofApp.h"
#include "ofMain.h"

//========================================================================
int main()
{
#ifdef OF_TARGET_OPENGLES
    ofGLESWindowSettings settings;
    settings.glesVersion = 3;
#else
    ofGLWindowSettings settings;
    settings.setGLVersion(4, 6);
    settings.setSize(1920, 1080);
#endif

    //     // this kicks off the running of my app
    //     // can be OF_WINDOW or OF_FULLSCREEN
    //     // pass in width and height too:
    ofCreateWindow(settings);

    ofRunApp(new ofApp());
}

#pragma once
#include "ofMain.h"

class gestureFinder {
public:
    //! This class contains a set of tools to calculate motion derivatives for values of `float` or `glm::vec2`
    //! Extend or overload the member functions to work with higher order vectors such as `glm::vec3` following the overloaded example of `derive(glm::vec2)` member function
    gestureFinder();
    // gestureFinder(gestureFinder&&) = default;
    // gestureFinder(const gestureFinder&) = default;
    // gestureFinder& operator=(gestureFinder&&) = default;
    // gestureFinder& operator=(const gestureFinder&) = default;
    ~gestureFinder();

    //! A simple data structure to contain all relevant derivative values
    //! `gestureFinder::derivatives::firstDt` and `gestureFinder::derivatives::secondDt` store the velocity and acceleration respectively
    //! `gestureFinder::derivatives::oldFirst` and `gestureFinder::derivatives::oldSecond` store the previous values for velocity and acceleration respectively
    //! `gestureFinder::derivatives::period` sets the measurement period for average calculation
    //! `gestureFinder::derivatives::averageDt` and `gestureFinder::derivatives::averageSecondDt` store the average velocity and acceleration respectively
    struct derivatives {
        //! Current velocity
        float firstDt;
        //! Current acceleration
        float secondDt;
        //! Previous velocity
        float oldFirst;
        //! Previous acceleration
        float oldSecond;
        //! Duration to meaure averages
        unsigned int period;
        //! Current index in the duration cycle
        unsigned int count;
        //! Average velocity
        double averageDt;
        //! Average acceleration
        double averageSecondDt;
    };

    //! Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop.
    //! It stores the previous value and compares it against the current value
    void derive(float value);
    //! Generate and store the derivatives for the given 2D value i.e glm::vec2. This member function is ideally called inside a `while` loop.
    //! It stores the previous value and compares it against the current value
    void derive(glm::vec2 value);

    //! Calculates and stores the average for the value set in `gestureFinder::derive`.
    //! The time period for measurement is set from `gestureFinder::m_Deriv.period` which can be set from `gestureFinder::setPeriod`
    void average();

    //! Returns the current velocity calculated from the most recent `gestureFinder:derive` call.
    float velocity();
    //! Returns the current acceleartion calculated from the most recent `gestureFinder:derive` call.
    float acceleartion();
    //! Returns the previous velocity calculated from the most recent `gestureFinder:derive` call.
    float oldValue();
    //! Returns the previous acceleration calculated from the most recent `gestureFinder:derive` call.
    float oldDt();

    //! Sets the period for the calculation of average value by `gestureFinder::average`
    inline void setPeriod(unsigned int period)
    {
        m_Deriv.period = period;
    }

    //! Returns the average velocity calculated by `gestureFinder::average`
    double avgVelocity();
    //! Returns the average acceleartion calculated by `gestureFinder::average`
    double avgAccel();

private:
    derivatives m_Deriv;
    double tempFirst, tempSecond;
};

#include "gestureFinder.h"

gestureFinder::gestureFinder()
{
    m_Deriv.oldFirst = 0;
    m_Deriv.oldSecond = 0;
    m_Deriv.firstDt = 0;
    m_Deriv.secondDt = 0;
    m_Deriv.period = 0;
    m_Deriv.count = 0;
    m_Deriv.averageDt = 0;
    m_Deriv.averageSecondDt = 0;
    tempFirst = 0;
    tempSecond = 0;
}

gestureFinder::~gestureFinder()
{
}

void gestureFinder::derive(float value)
{
    m_Deriv.firstDt = value - m_Deriv.oldFirst;
    m_Deriv.secondDt = m_Deriv.firstDt - m_Deriv.oldSecond;
    m_Deriv.oldFirst = value;
    m_Deriv.oldSecond = m_Deriv.firstDt;
}

void gestureFinder::derive(glm::vec2 value)
{
    float tempValue = glm::length(value);
    m_Deriv.firstDt = tempValue - m_Deriv.oldFirst;
    m_Deriv.secondDt = m_Deriv.firstDt - m_Deriv.oldSecond;
    m_Deriv.oldFirst = tempValue;
    m_Deriv.oldSecond = m_Deriv.firstDt;
}

void gestureFinder::average()
{
    if (m_Deriv.count < m_Deriv.period) {
        tempFirst += m_Deriv.firstDt;
        tempSecond += m_Deriv.secondDt;
        m_Deriv.count++;
    } else {
        m_Deriv.averageDt = tempFirst;
        m_Deriv.averageSecondDt = tempSecond;
        m_Deriv.count = 0;
    }
}

float gestureFinder::velocity()
{
    return m_Deriv.firstDt;
}

float gestureFinder::acceleartion()
{
    return m_Deriv.secondDt;
}

float gestureFinder::oldValue()
{
    return m_Deriv.oldFirst;
}

float gestureFinder::oldDt()
{
    return m_Deriv.oldSecond;
}

double gestureFinder::avgAccel()
{
    return m_Deriv.averageSecondDt;
}

double gestureFinder::avgVelocity()
{
    return m_Deriv.averageDt;
}
